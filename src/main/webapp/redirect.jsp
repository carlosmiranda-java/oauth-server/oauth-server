<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<body>
<%
	String success_url = request.getParameter("success_url");
    String access_token = ""+request.getAttribute("access_token");
    String refresh_token = ""+request.getAttribute("refresh_token");
    String metodo = ""+request.getAttribute("metodo");

    request.removeAttribute("access_token");
    request.removeAttribute("refresh_token");
    request.removeAttribute("metodo");

    if(metodo == null ){
    	metodo = "post";
    }else if(metodo.length() == 0){
    	metodo = "post";
    }else if(!metodo.equalsIgnoreCase("get")){
    	metodo = "post";
    }
%>

<form name="f1" id="f1" action="<%=success_url%>" enctype="application/x-www-form-urlencoded" method="<%=metodo%>">
<br><input type="hidden" name="access_token" value="<%=access_token%>"/>
<br><input type="hidden" name="refresh_token" value="<%=refresh_token%>"/>
</form>
<script type="text/javascript">

    
        document.f1.submit();
    

</script>
</body>
</html>
