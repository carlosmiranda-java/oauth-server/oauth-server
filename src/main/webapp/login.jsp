<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>Login</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
	integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
	crossorigin="anonymous">
<link rel="stylesheet" href="./login.css">
</head>
<body>

<%
	String success_url = request.getParameter("success_url");
    String mensaje = request.getParameter("mensaje");
    String metodo = request.getParameter("metodo");
    
    
%>
	<form class="float-center fachada" action="/oauth-server/doLogin" enctype="application/x-www-form-urlencoded" method="post">
		<input type="hidden" name="success_url" value="<%=success_url%>"/>
		
<input type="hidden" name="error_url" value="/oauth-server/login.jsp"/>
<input type="hidden" name="metodo" value="<%=metodo%>"/>
	<%if("0".equalsIgnoreCase(mensaje)){%>
			<span style="color: red; display: block; width: 100%; align: center">Usuario o password incorrectos</span>
		<%} %>	
		<div class="login-block">
			<h5>Login</h5>
			<hr>
		</div>
		<div class="form-group">
			<label for="username">Usuario:</label><span style="color: red;">*</span>
			<input type="text" class="form-control" id="username" name="username"
				placeholder="Usuario" required>
		</div>
		<div class="form-group">
			<label for="password">Password</label><span style="color: red;">*</span>
			<input type="password" class="form-control" id="password" name="password"
				placeholder="Contraseņa" required>
		</div>


		<button [disabled]="loginForm.invalid" type="submit"
			class="btn btn-secondary">Entrar</button>
			
		
		

	</form>
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
		integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"
		integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut"
		crossorigin="anonymous"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"
		integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k"
		crossorigin="anonymous"></script>
</body>
</html>


