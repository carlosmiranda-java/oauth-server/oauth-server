package my.oauthserver.config;

import org.springframework.security.oauth2.common.OAuth2RefreshToken;

public class CustomOAuth2RefreshToken implements OAuth2RefreshToken{

	private String value;
	@Override
	public String getValue() {
		return value;
	}
	
	public void setValue(String value) {
		this.value= value;
	}

}
