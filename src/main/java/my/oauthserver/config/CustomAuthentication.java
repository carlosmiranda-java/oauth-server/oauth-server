package my.oauthserver.config;

import java.util.Collection;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

public class CustomAuthentication implements Authentication{

	
	private Object details;
	private String name;
	private Collection<? extends GrantedAuthority> authorities;
	private Object credentials;
	private Object principal;
	private boolean authenticated;
	
	public CustomAuthentication(Authentication auth){
		this.name = auth.getName();
		this.details = auth.getDetails();
		this.authorities = auth.getAuthorities();
		this.credentials = auth.getCredentials();
		this.principal = auth.getPrincipal();
		this.authenticated = auth.isAuthenticated();
	}
	
	@Override
	public String getName() {
		
		return this.name;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		
		return this.authorities;
	}

	@Override
	public Object getCredentials() {

		return credentials;
	}

	@Override
	public Object getDetails() {
		return this.details;
	}
	public void setDetails(Object c) {
		this.details = c;
	}

	@Override
	public Object getPrincipal() {
		
		return this.principal;
	}

	@Override
	public boolean isAuthenticated() {
		
		return this.authenticated;
	}

	@Override
	public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {
		this.authenticated = isAuthenticated;
	}
	

}
