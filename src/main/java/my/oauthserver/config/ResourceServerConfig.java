package my.oauthserver.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.error.OAuth2AccessDeniedHandler;

import my.oauthserver.util.CustomTokenExtractor;




@Configuration
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {

    private static final String RESOURCE_ID = "resource_id";
    
    @Autowired
	private CustomTokenExtractor tokenExtractor;
	
   
   
    @Override
    public void configure(ResourceServerSecurityConfigurer resources) {
        resources.resourceId(RESOURCE_ID).stateless(false);
        resources.tokenExtractor(tokenExtractor);
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
    	
    	
    	http.
		anonymous().disable()
		.requestMatchers().antMatchers("/secured/**")
		.and().authorizeRequests()
		.antMatchers(HttpMethod.OPTIONS,"**").permitAll()
		.antMatchers("/**").authenticated().and()
		.exceptionHandling().accessDeniedHandler(new OAuth2AccessDeniedHandler());
    	
    }
   
    
   

    

}