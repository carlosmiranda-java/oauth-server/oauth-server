package my.oauthserver.config;

import org.springframework.security.core.GrantedAuthority;

public class CustomGrantedAuthority implements GrantedAuthority{

	private String authority;
	
	
	public CustomGrantedAuthority(String authority){
		this.authority = authority;
	}
	
	public String getAuthority() {
		
		return this.authority;
	}

}
