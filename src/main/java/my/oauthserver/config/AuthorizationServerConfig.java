package my.oauthserver.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.approval.TokenStoreUserApprovalHandler;
import org.springframework.security.oauth2.provider.request.DefaultOAuth2RequestFactory;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.InMemoryTokenStore;




@Configuration
@EnableAuthorizationServer
public class AuthorizationServerConfig extends AuthorizationServerConfigurerAdapter {


	@Value("${security.oauth.client.id}")
	private String clientId;
	
	@Value("${security.oauth.client.password}")
	private String clientPassword;
	
	@Value("${security.oauth.accesstoken.timeout}")
	private int accessTokenTimeOut;
	
	@Value("${security.oauth.refreshtoken.timeout}")
	private int refreshTokenTimeOut;
	
	

	
	
    
    @Autowired
    private ClientDetailsService clientDetailsService;


   
    @Autowired
	private CustomAuthenticationManager authenticationManager;
	
    
    
    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {

    	
    	
    	clients.inMemory()
        .withClient(clientId)
        .authorizedGrantTypes("password", "authorization_code", "refresh_token", "implicit")
        .authorities("ROLE_CLIENT", "ROLE_TRUSTED_CLIENT")
        .scopes("read", "write", "trust")
        .secret(clientPassword)
        .resourceIds("resource_id")
        .accessTokenValiditySeconds(accessTokenTimeOut)
        .refreshTokenValiditySeconds(refreshTokenTimeOut)
       ;
    
    	
    }

    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) {
        endpoints
        .tokenStore(tokenStore())
        .tokenServices(tokenServices())
        .authenticationManager(authenticationManager);        
        
    }
    
    @Bean
    public DefaultTokenServices tokenServices(){
    	DefaultTokenServices r = new DefaultTokenServices();
    	r.setTokenStore(tokenStore());
    	r.setSupportRefreshToken(true);
    	r.setRefreshTokenValiditySeconds(refreshTokenTimeOut);
    	r.setAccessTokenValiditySeconds(accessTokenTimeOut);
    
    	return r;
    }
    
    @Override
    public void configure(AuthorizationServerSecurityConfigurer oauth) throws Exception {
      oauth.checkTokenAccess("isAuthenticated()");
      oauth.allowFormAuthenticationForClients();
      
      
      
    }
   
//    @Bean
//    public TokenStore tokenStore() {
//        return new InMemoryTokenStore();
//    }
    
    @Bean
	public TokenStore tokenStore() {
		
		
		return new InMemoryTokenStore();
		
	}
    
   
    
    @Bean
    @Autowired
    public TokenStoreUserApprovalHandler userApprovalHandler(TokenStore tokenStore) {
    	
    	
        TokenStoreUserApprovalHandler handler = new TokenStoreUserApprovalHandler();
        handler.setTokenStore(tokenStore);
        handler.setRequestFactory(new DefaultOAuth2RequestFactory(clientDetailsService));
        return handler;
    }

    @Bean
    public static NoOpPasswordEncoder passwordEncoder() {
     return (NoOpPasswordEncoder) NoOpPasswordEncoder.getInstance();
    }
    
    
   

    
    
   
	
	
    
  
}