package my.oauthserver.config;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import my.oauthserver.common.Respuesta;


@Component
public class CustomAuthenticationManager implements AuthenticationManager {

	
//	@Autowired
//	private UserDetailsService userDetailsService;

	private static Logger log = LoggerFactory.getLogger(CustomAuthenticationManager.class);
	private static String ON_BEHALF_OF_GROUP="RENTAS_ON_BEHALF_OF_API";
	
	
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		log.info("");
		
		
		GrantedAuthority rolCustom = new CustomGrantedAuthority("ROLE_CUSTOM");
		List<GrantedAuthority> list = new ArrayList<GrantedAuthority>();
		list.add(rolCustom);
		
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes())
				.getRequest();
		String app = null;
		String password = null;
		String username = null;
		String from = null;
		String token = null;
		String sign = null;
		boolean error = false;
		
		Respuesta userDetails = null;		

		
		
			
			log.info("autenticando...");
			password = authentication.getCredentials().toString().trim();
			username = authentication.getName();
			log.info("user: " + username);
			log.debug("user: " + username);
				
				if(username != null && password !=null && username.compareTo(password)==0){
					
				}else{
					error = true;
				}
				
		

		if (error) {
			log.info("bad credentials exception for: "+username);
			throw new BadCredentialsException("bad credentials exception");
		}

		Authentication auth = new UsernamePasswordAuthenticationToken(username, null, list);
		CustomAuthentication c = new CustomAuthentication(auth);
		
		c.setDetails(userDetails);
		
		return c;
	}

}
