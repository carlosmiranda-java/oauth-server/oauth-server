package my.oauthserver;

import java.security.Principal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.DispatcherServlet;

import my.oauthserver.common.Respuesta;
import my.oauthserver.config.CustomAuthentication;
import my.oauthserver.util.CookieManager;
import my.oauthserver.util.CustomTokenExtractor;


@SpringBootApplication
@RestController
@ServletComponentScan
@ComponentScan({"my.oauthserver"})
@EnableAutoConfiguration(exclude={DataSourceAutoConfiguration.class})
@PropertySource("classpath:${RUNTIME_ENVIRONMENT}.properties")
public class SpringBootTomcatApplication extends SpringBootServletInitializer{

	
	private static Logger log = LoggerFactory.getLogger(SpringBootTomcatApplication.class);

	@Autowired
	private CustomTokenExtractor tokenExtractor;
	
	@Autowired
	private CookieManager cookieManager;
	
	
	public static void main(String[] args) {
		SpringApplication.run(SpringBootTomcatApplication.class, args);
	}
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {

		builder.properties("spring.config.name:${RUNTIME_ENVIRONMENT}");
		
		return builder;
	}
	
	@Bean
	DispatcherServlet dispatcherServlet() {
		DispatcherServlet ds = new DispatcherServlet();
		ds.setThrowExceptionIfNoHandlerFound(true);
		return ds;
	}
	
	
	
	@RequestMapping("/secured/user")
	public Principal user(Principal user) {
		
		log.info("");
		log.debug(user.toString());
		
		
		return user;
	}
	@RequestMapping("/secured/autenticacion")
	public Authentication user(Authentication user) {
		log.info("");
		log.debug(user.toString());
		return user;
	}
	@RequestMapping("/secured/detalles")
	public Respuesta detalles(Authentication user) {
		log.info("");
		log.debug(user.toString());
		OAuth2Authentication auth =(OAuth2Authentication)user;
		CustomAuthentication cus = (CustomAuthentication)auth.getUserAuthentication();
		Respuesta r = (Respuesta) cus.getDetails();
		
		log.debug(r.toString());
		log.info("");
		
		return r;
	}
	@RequestMapping("/healthz")
	public Respuesta health() {
		log.info("");
		Respuesta r = new Respuesta();
		r.setBody("healthz");
		
		log.info("");
		
		return r;
	}
}
