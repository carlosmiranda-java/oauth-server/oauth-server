package my.oauthserver.util;

import org.springframework.context.annotation.Bean;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.authentication.TokenExtractor;


@EnableResourceServer
public abstract class CustomResourceServerApplication extends ResourceServerConfigurerAdapter{

	
	
	public void configure(ResourceServerSecurityConfigurer resources) {
	    resources.tokenExtractor(tokenExtractor());
	}
	
	@Bean
	public TokenExtractor tokenExtractor(){
		return new CustomTokenExtractor();
	}
	@Bean
	public CookieManager cookieManager(){
		return new CookieManager();
	}
}
