package my.oauthserver.util;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.provider.authentication.BearerTokenExtractor;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;


@Component
public class CustomTokenExtractor extends BearerTokenExtractor{

	@Autowired
	private CookieManager cookieManager;
	
	
	@Override
	protected String extractToken(HttpServletRequest request) {
		//esta el access token
		if(request.getParameter("access_token")!= null){
			return request.getParameter("access_token");
		}
		//si no viene por parametro, lo buscamos en la cookie
		Cookie c = cookieManager.getCookieByName(request);
		 
		
		if(c!= null){
			
			try {
				String valor = CustomEncrypt.Desencriptar(c.getValue());
				ObjectMapper objectMapper = new ObjectMapper();
				JsonNode jsonNode = objectMapper.readTree(valor);
				
				
				
				return jsonNode.get("access_token").asText();
						
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		
		return super.extractToken(request);
	}
	
	

}
