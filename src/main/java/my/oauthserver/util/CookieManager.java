package my.oauthserver.util;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class CookieManager {

	
	public static String COOKIE="RENTASOAUTH";
	
	@Value("${cookie.maxAge}")
	public int MAX_AGE;
	@Value("${cookie.domain}")
	public String DOMAIN;
	
	public void addCookie(HttpServletResponse response, String value){
		
		response.addCookie(getCookie(COOKIE, CustomEncrypt.Encriptar(value)));
	}
	
	public Cookie getCookieByName(HttpServletRequest request){
		Cookie[] cookies = request.getCookies();

		if (cookies != null) {
		 for (Cookie cookie : cookies) {
		   if (cookie.getName().equals(COOKIE)) {
		     return cookie;
		    }
		  }
		}
		return null;
		
	}
	
	private Cookie getCookie(String name, String value){
		Cookie c = new Cookie(name, value);
		c.setDomain(DOMAIN);
		c.setMaxAge(MAX_AGE);
		c.setPath("/");
		c.setSecure(true);
		return c;
	}
}
