package my.oauthserver;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.Socket;
import java.net.URL;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jndi.JndiTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping(value = "/")
public class ItsWorking {
	
	private Resource resource; 
	private Properties props;
	
	private static Logger log = LogManager.getLogger(ItsWorking.class);

	private static String SITE="site";
	private static String DB="db";
	private static String DS="ds";
	private static String SOCKET="socket";

	private static int SITE_COUNT= 0;
	private static int DB_COUNT= 0;
	private static int DS_COUNT= 0;
	private static int SOCKET_COUNT= 0;

	
	
	@PostConstruct
	public void postConstruct(){
		resource = new ClassPathResource("/itsworking."+System.getenv("RUNTIME_ENVIRONMENT")+".properties");
		try {
			props =   PropertiesLoaderUtils.loadProperties(resource);
			
			if(props.get(this.DB+".count")!=null){
				this.DB_COUNT = Integer.parseInt(""+props.get(this.DB+".count"));
			}
			if(props.get(this.DS+".count")!=null){
				this.DS_COUNT = Integer.parseInt(""+props.get(this.DS+".count"));
			}
			if(props.get(this.SITE+".count")!=null){
				this.SITE_COUNT = Integer.parseInt(""+props.get(this.SITE+".count"));
			}
			if(props.get(this.SOCKET+".count")!=null){
				this.SOCKET_COUNT = Integer.parseInt(""+props.get(this.SOCKET+".count"));
			}
			log.info("se cargo el archivo de propiedades");
		} catch (IOException e) {
			log.error("error al cargar archivo de propiedades");
		}
		
	}
	
	@GetMapping("itsworking")
	public ResponseEntity itsWorking(){
		String retorno="";
		String ok="";
		if(props!= null){
			
			
			
			
			
			for(int x = 1; x <= this.SITE_COUNT;x++){
				String url = props.getProperty(this.SITE+x+".url");
				if(url == null){
					x = this.SITE_COUNT;
				}else{
					retorno = testUlr(url);
					if(retorno != null){
						return new ResponseEntity<String>(retorno, HttpStatus.SERVICE_UNAVAILABLE);	
					}else{
						ok+="OK! "+url+"\n";
					}
				}
			}
			for(int x = 1; x <= this.DB_COUNT;x++){
				String string = props.getProperty(this.DB+x+".string");
				String user = props.getProperty(this.DB+x+".user");
				String password = props.getProperty(this.DB+x+".password");
				String sql = props.getProperty(this.DB+x+".sql");
				String driver = props.getProperty(this.DB+x+".driver");

				
				if(string == null){
					x = this.DB_COUNT;
				}else{
					retorno =testDB(string, user, password, sql, driver);
					if(retorno != null){
						return new ResponseEntity<String>(retorno, HttpStatus.SERVICE_UNAVAILABLE);	
					}else{
						ok+="OK! "+string+"\n";
					}
				}
			}
			for(int x = 1; x <= this.DS_COUNT;x++){
				String jndi = props.getProperty(this.DS+x+".jndi");
				String sql = props.getProperty(this.DS+x+".sql");
				

				
				if(jndi == null){
					x = this.DS_COUNT;
				}else{
					retorno =testDS(jndi,sql);
					if(retorno != null){
						return new ResponseEntity<String>(retorno, HttpStatus.SERVICE_UNAVAILABLE);	
					}else{
						ok+="OK! "+jndi+"\n";
					}
				}
			}
			for(int x = 1; x <= this.SOCKET_COUNT;x++){
				String ip = props.getProperty(this.SOCKET+x+".ip");
				String port = props.getProperty(this.SOCKET+x+".port");


				
				if(ip == null){
					x = this.SOCKET_COUNT;
				}else{
					retorno =testIp(ip,port);
					if(retorno != null){
						return new ResponseEntity<String>(retorno, HttpStatus.SERVICE_UNAVAILABLE);	
					}else{
						ok+="OK! "+ip+":"+port+"\n";
					}
				}
			}
			
		}else{
			retorno ="no se encontr� archivo de propiedades";
		}
		
		
		return new ResponseEntity<String>(ok, HttpStatus.OK);
	}
	
	
	private String testUlr(String url){
		
		try {
	        URL dir = new URL(url);
	        HttpURLConnection urlConn = (HttpURLConnection) dir.openConnection();
	        urlConn.connect();
	        
	        return null;
	        
	    } catch (IOException e) {
	        return "error al acceder a: "+url;
	        		
	    }

	}
private String testIp(String ip, String port){
		
	try {
		Socket socket = new Socket(ip, Integer.parseInt(port));
		socket.close();
	} catch (UnknownHostException e) {
		return ip + ":"+port+" NOT reachable. ("+e.getMessage()+")";
	} catch (IOException e) {
		return ip + ":"+port+" NOT reachable. ("+e.getMessage()+")";
	}
	return null;
	
	
		
	
	}
	private String testDB(String string, String user, String password, String sql, String driver){
		
		Connection conn = null;
		Statement stmt = null;
		String retorno = null;
		
		try {
            Class.forName(driver); 

			
			conn = DriverManager.getConnection(string, user, password);
			stmt = conn.createStatement();
			boolean x = stmt.execute(sql);
			stmt.close();
			conn.close();
		} catch (SQLException e) {
			retorno = "error al acceder a: "+ string +"("+e.getMessage()+")";
		} catch (ClassNotFoundException e) {
			retorno = "error al acceder a: "+ string +"("+e.getMessage()+")";

		}finally{
			
			try{
				if(stmt != null){
					stmt.close();
				}
			}catch(Exception e){}
			
			try{
				if(conn != null){
					conn.close();
				}
			}catch(Exception e){}
		}
		
		
		
		return retorno;

	}
	
private String testDS(String jndi, String sql){
		
		Connection conn = null;
		Statement stmt = null;
		String retorno = null;
		
		try {
			
			DataSource ds = (DataSource) new JndiTemplate().lookup(jndi);
			

			
			conn = ds.getConnection();
			stmt = conn.createStatement();
			boolean x = stmt.execute(sql);
			stmt.close();
			conn.close();
		} catch (SQLException e) {
			retorno = "error al acceder a: "+ jndi +"("+e.getMessage()+")";
		} catch (NamingException e) {
			retorno = "error al acceder a: "+ jndi +"("+e.getMessage()+")";

		} finally{
			
			try{
				if(stmt != null){
					stmt.close();
				}
			}catch(Exception e){}
			
			try{
				if(conn != null){
					conn.close();
				}
			}catch(Exception e){}
		}
		
		
		
		return retorno;

	}
	
}
