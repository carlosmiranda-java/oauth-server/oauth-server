package my.oauthserver.common;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

public class AbstractServlet extends HttpServlet {

    protected AutowireCapableBeanFactory autowireBean;

    @Override
    public void init() throws ServletException {
        super.init();
        WebApplicationContext context = WebApplicationContextUtils
                .getWebApplicationContext(getServletContext());
        autowireBean = context.getAutowireCapableBeanFactory();
        autowireBean.autowireBean(this);
    }
}