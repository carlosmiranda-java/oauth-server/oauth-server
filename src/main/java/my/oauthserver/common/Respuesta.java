package my.oauthserver.common;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Respuesta implements Serializable{
	
	@Override
	public String toString() {
		
		return "cuit:"+this.cuit+"\n origen: "+origen+"\n body: "+body;
	}



	private static final long serialVersionUID = -3077410876218845434L;
	
	private String cuit;
	private String userName;
	private List<String> representados;
	private String body;
	private Origen origen;
	
	
	public Respuesta(){
		representados = new ArrayList<String>();
	}

	

	public String getCuit() {
		return cuit;
	}

	public void setCuit(String cuit) {
		this.cuit = cuit;
	}

	public List<String> getRepresentados() {
		return representados;
	}

	public void setRepresentados(List<String> representados) {
		this.representados = representados;
	}
	
	public void addRepresentado(String cuit){
			this.representados.add(cuit);
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}



	public Origen getOrigen() {
		return origen;
	}



	public void setOrigen(Origen origen) {
		this.origen = origen;
	}



	public String getUserName() {
		return userName;
	}



	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	
	
}
